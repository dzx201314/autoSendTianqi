package com.tianqi.autoSendTianqi.controller;

import com.alibaba.fastjson.JSON;
import com.github.qcloudsms.SmsSingleSender;
import com.github.qcloudsms.SmsSingleSenderResult;
import com.github.qcloudsms.httpclient.HTTPException;
import com.tianqi.autoSendTianqi.util.HttpUtils;
import com.tianqi.autoSendTianqi.util.MailUtils;
import com.tianqi.autoSendTianqi.vo.Friend;
import com.tianqi.autoSendTianqi.vo.FriendList;
import com.tianqi.autoSendTianqi.vo.Tianqi;
import com.tianqi.autoSendTianqi.vo.Tianqi.WeatherBeanX;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.FastDateFormat;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TianqiController {

    private static final Logger log = LoggerFactory.getLogger(TianqiController.class);
    @Autowired
    private MailUtils mail;


    @Scheduled(
            cron = "0 0 8 * * ?"
    )
    public void find() {
        String url = "http://tq.360.cn/api/weatherquery/querys?app=tq360&code=101020100&t=201565159968966&c=201565260989066";
        String json = HttpUtils.httpGet(url);
        json = json.substring(9, json.length() - 1);
        Tianqi tianqi = (Tianqi)JSON.parseObject(json, Tianqi.class);
        List<Friend> fs = FriendList.getFriends();
        this.sendPhone(tianqi, fs);
        this.sendMail(tianqi, fs);
    }

    @GetMapping({"tianqi"})
    public void find1() {
        String url = "http://tq.360.cn/api/weatherquery/querys?app=tq360&code=101020100&t=201565159968966&c=201565260989066";
        String json = HttpUtils.httpGet(url);
        json = json.substring(9, json.length() - 1);
        log.info(json);
        Tianqi tianqi = (Tianqi)JSON.parseObject(json, Tianqi.class);
        List fs = FriendList.getFriends();
        this.sendMail(tianqi, fs);
    }

    private void sendMail(Tianqi tianqi, List<Friend> fs) {
        String result = this.getResult(tianqi);
        log.info(result);
        String curr = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(new Date());
       for (Friend f : fs) {
           if (StringUtils.isNotBlank(f.getEmail())) {
               this.mail.sendSimpleMail(f.getEmail(), curr + "你的出行管家", result);
           }

       }
    }

    private void sendPhone(Tianqi tianqi, List fs) {
        ArrayList result1 = this.getResult1(tianqi);

        try {
            SmsSingleSender e = new SmsSingleSender(1400207953, "dcf8015432958a158b0a40b0c45e6706");
            Iterator var6 = fs.iterator();

            while(var6.hasNext()) {
                Friend f = (Friend)var6.next();
                if(StringUtils.isNotBlank(f.getPhone())) {
                    SmsSingleSenderResult result2 = e.sendWithParam("86", f.getPhone(), 391359, result1, (String)null, "", "");
                    log.info("" + result2);
                }
            }
        } catch (HTTPException var8) {
            var8.printStackTrace();
        } catch (JSONException var9) {
            var9.printStackTrace();
        } catch (IOException var10) {
            var10.printStackTrace();
        }

    }

    public static void main(String[] args) {
        String curr = FastDateFormat.getInstance("yyyy-MM-dd HH:mm:ss").format(new Date());
        System.out.println(curr);
    }

    private String getResult(Tianqi tianqi) {
        String city = (String)((List)tianqi.getArea().get(0)).get(0);
        String pm = tianqi.getPm25().getAdvice();
        String temperature = tianqi.getRealtime().getWeather().getTemperature();
        String direct = tianqi.getRealtime().getWind().getDirect();
        String info = tianqi.getRealtime().getWeather().getInfo();
        String wendu = "";
        List weather = tianqi.getWeather();
        Iterator tiexin = weather.iterator();

        while(tiexin.hasNext()) {
            WeatherBeanX chuanyi = (WeatherBeanX)tiexin.next();
            if(chuanyi.getDate().equals(tianqi.getRealtime().getDate())) {
                wendu = wendu + (String)chuanyi.getInfo().getNight().get(2);
                wendu = wendu + "℃  ~ ";
                wendu = wendu + (String)chuanyi.getInfo().getDay().get(2) + "℃";
                break;
            }
        }

        String chuanyi1 = (String)tianqi.getLife().getInfo().getChuanyi().get(1);
        String tiexin1 = (String)tianqi.getLife().getInfo().getDaisan().get(1);
        String yundong = (String)tianqi.getLife().getInfo().getYundong().get(1);
        String ziwaixian = (String)tianqi.getLife().getInfo().getZiwaixian().get(1);
        String result = "❤    早上好！这是今天的天气预报  \n❤    你的城市: %s\n❤    Pm值        : %s\n❤    当前温度: %s\n❤    风向     : %s\n❤    天气     : %s\n❤    温度     : %s\n❤    穿衣      : %s\n❤    我很贴心: %s\n❤    运动      : %s\n❤    紫外线  : %s\n";
        result = String.format(result, new Object[]{city, pm, temperature, direct, info, wendu, chuanyi1, tiexin1, yundong, ziwaixian});
        return result;
    }

    private ArrayList getResult1(Tianqi tianqi) {
        ArrayList params = new ArrayList();
        String pm = tianqi.getPm25().getPm25() + " " + tianqi.getPm25().getQuality();
        String temperature = tianqi.getRealtime().getWeather().getTemperature() + "度";
        String direct = tianqi.getRealtime().getWind().getDirect();
        String info = tianqi.getRealtime().getWeather().getInfo();
        String wendu = "";
        List weather = tianqi.getWeather();
        Iterator var10 = weather.iterator();

        while(var10.hasNext()) {
            WeatherBeanX w = (WeatherBeanX)var10.next();
            if(w.getDate().equals(tianqi.getRealtime().getDate())) {
                wendu = wendu + (String)w.getInfo().getNight().get(2);
                wendu = wendu + "度";
                wendu = wendu + (String)w.getInfo().getDay().get(2) + "度";
                break;
            }
        }

        params.add(pm);
        params.add(temperature);
        params.add(direct);
        params.add(info);
        params.add(wendu);
        return params;
    }
}