package com.tianqi.autoSendTianqi.vo;

import lombok.Data;

@Data
public class Friend
{
    private Integer id;
    private String name;
    private String email;
    private String phone;

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    public String toString()
    {
        return "Friend(id=" + getId() + ", name=" + getName() + ", email=" + getEmail() + ", phone=" + getPhone() + ")";
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $email = getEmail();result = result * 59 + ($email == null ? 43 : $email.hashCode());Object $phone = getPhone();result = result * 59 + ($phone == null ? 43 : $phone.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof Friend;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setEmail(String email)
    {
        this.email = email;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Friend)) {
            return false;
        }
        Friend other = (Friend)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$name = getName();Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        Object this$email = getEmail();Object other$email = other.getEmail();
        if (this$email == null ? other$email != null : !this$email.equals(other$email)) {
            return false;
        }
        Object this$phone = getPhone();Object other$phone = other.getPhone();return this$phone == null ? other$phone == null : this$phone.equals(other$phone);
    }

    public Friend(Integer id, String name, String email, String phone)
    {
        this.id = id;this.name = name;this.email = email;this.phone = phone;
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getEmail()
    {
        return this.email;
    }

    public String getPhone()
    {
        return this.phone;
    }

    public Friend() {}
}
