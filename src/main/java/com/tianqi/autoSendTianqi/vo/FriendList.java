package com.tianqi.autoSendTianqi.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
@Data
public class FriendList
{
    public static List<Friend> getFriends()
    {
        List<Friend> friends = new ArrayList();

        Friend f4 = new Friend(Integer.valueOf(2), "雪儿", "15636331627@163.com", "17601321021");

        friends.add(f4);
        return friends;
    }
}
