package com.tianqi.autoSendTianqi.vo;

import lombok.Data;

import java.util.List;
@Data
public class Tianqi
{
    private LifeBean life;
    private RealtimeBean realtime;
    private Pm25Bean pm25;
    private List<List<String>> area;
    private List<WeatherBeanX> weather;

    public LifeBean getLife()
    {
        return this.life;
    }

    public void setLife(LifeBean life)
    {
        this.life = life;
    }

    public RealtimeBean getRealtime()
    {
        return this.realtime;
    }

    public void setRealtime(RealtimeBean realtime)
    {
        this.realtime = realtime;
    }

    public Pm25Bean getPm25()
    {
        return this.pm25;
    }

    public void setPm25(Pm25Bean pm25)
    {
        this.pm25 = pm25;
    }

    public List<List<String>> getArea()
    {
        return this.area;
    }

    public void setArea(List<List<String>> area)
    {
        this.area = area;
    }

    public List<WeatherBeanX> getWeather()
    {
        return this.weather;
    }

    public void setWeather(List<WeatherBeanX> weather)
    {
        this.weather = weather;
    }

    public static class LifeBean
    {
        private String date;
        private InfoBean info;

        public String getDate()
        {
            return this.date;
        }

        public void setDate(String date)
        {
            this.date = date;
        }

        public InfoBean getInfo()
        {
            return this.info;
        }

        public void setInfo(InfoBean info)
        {
            this.info = info;
        }

        public static class InfoBean
        {
            private List<String> kongtiao;
            private List<String> daisan;
            private List<String> ziwaixian;
            private List<String> yundong;
            private List<String> ganmao;
            private List<String> xiche;
            private List<String> diaoyu;
            private List<String> guomin;
            private List<String> wuran;
            private List<String> chuanyi;

            public List<String> getKongtiao()
            {
                return this.kongtiao;
            }

            public void setKongtiao(List<String> kongtiao)
            {
                this.kongtiao = kongtiao;
            }

            public List<String> getDaisan()
            {
                return this.daisan;
            }

            public void setDaisan(List<String> daisan)
            {
                this.daisan = daisan;
            }

            public List<String> getZiwaixian()
            {
                return this.ziwaixian;
            }

            public void setZiwaixian(List<String> ziwaixian)
            {
                this.ziwaixian = ziwaixian;
            }

            public List<String> getYundong()
            {
                return this.yundong;
            }

            public void setYundong(List<String> yundong)
            {
                this.yundong = yundong;
            }

            public List<String> getGanmao()
            {
                return this.ganmao;
            }

            public void setGanmao(List<String> ganmao)
            {
                this.ganmao = ganmao;
            }

            public List<String> getXiche()
            {
                return this.xiche;
            }

            public void setXiche(List<String> xiche)
            {
                this.xiche = xiche;
            }

            public List<String> getDiaoyu()
            {
                return this.diaoyu;
            }

            public void setDiaoyu(List<String> diaoyu)
            {
                this.diaoyu = diaoyu;
            }

            public List<String> getGuomin()
            {
                return this.guomin;
            }

            public void setGuomin(List<String> guomin)
            {
                this.guomin = guomin;
            }

            public List<String> getWuran()
            {
                return this.wuran;
            }

            public void setWuran(List<String> wuran)
            {
                this.wuran = wuran;
            }

            public List<String> getChuanyi()
            {
                return this.chuanyi;
            }

            public void setChuanyi(List<String> chuanyi)
            {
                this.chuanyi = chuanyi;
            }
        }
    }

    public static class RealtimeBean
    {
        private String mslp;
        private WindBean wind;
        private String time;
        private String pressure;
        private WeatherBean weather;
        private String feelslike_c;
        private String dataUptime;
        private String date;

        public String getMslp()
        {
            return this.mslp;
        }

        public void setMslp(String mslp)
        {
            this.mslp = mslp;
        }

        public WindBean getWind()
        {
            return this.wind;
        }

        public void setWind(WindBean wind)
        {
            this.wind = wind;
        }

        public String getTime()
        {
            return this.time;
        }

        public void setTime(String time)
        {
            this.time = time;
        }

        public String getPressure()
        {
            return this.pressure;
        }

        public void setPressure(String pressure)
        {
            this.pressure = pressure;
        }

        public WeatherBean getWeather()
        {
            return this.weather;
        }

        public void setWeather(WeatherBean weather)
        {
            this.weather = weather;
        }

        public String getFeelslike_c()
        {
            return this.feelslike_c;
        }

        public void setFeelslike_c(String feelslike_c)
        {
            this.feelslike_c = feelslike_c;
        }

        public String getDataUptime()
        {
            return this.dataUptime;
        }

        public void setDataUptime(String dataUptime)
        {
            this.dataUptime = dataUptime;
        }

        public String getDate()
        {
            return this.date;
        }

        public void setDate(String date)
        {
            this.date = date;
        }

        public static class WindBean
        {
            private String windspeed;
            private String direct;
            private String power;

            public String getWindspeed()
            {
                return this.windspeed;
            }

            public void setWindspeed(String windspeed)
            {
                this.windspeed = windspeed;
            }

            public String getDirect()
            {
                return this.direct;
            }

            public void setDirect(String direct)
            {
                this.direct = direct;
            }

            public String getPower()
            {
                return this.power;
            }

            public void setPower(String power)
            {
                this.power = power;
            }
        }

        public static class WeatherBean
        {
            private String humidity;
            private String img;
            private String info;
            private String temperature;

            public String getHumidity()
            {
                return this.humidity;
            }

            public void setHumidity(String humidity)
            {
                this.humidity = humidity;
            }

            public String getImg()
            {
                return this.img;
            }

            public void setImg(String img)
            {
                this.img = img;
            }

            public String getInfo()
            {
                return this.info;
            }

            public void setInfo(String info)
            {
                this.info = info;
            }

            public String getTemperature()
            {
                return this.temperature;
            }

            public void setTemperature(String temperature)
            {
                this.temperature = temperature;
            }
        }
    }

    public static class Pm25Bean
    {
        private int so2;
        private int o3;
        private String co;
        private int level;
        private String color;
        private int no2;
        private int aqi;
        private String quality;
        private int pm10;
        private int pm25;
        private String advice;
        private String chief;
        private long upDateTime;

        public int getSo2()
        {
            return this.so2;
        }

        public void setSo2(int so2)
        {
            this.so2 = so2;
        }

        public int getO3()
        {
            return this.o3;
        }

        public void setO3(int o3)
        {
            this.o3 = o3;
        }

        public String getCo()
        {
            return this.co;
        }

        public void setCo(String co)
        {
            this.co = co;
        }

        public int getLevel()
        {
            return this.level;
        }

        public void setLevel(int level)
        {
            this.level = level;
        }

        public String getColor()
        {
            return this.color;
        }

        public void setColor(String color)
        {
            this.color = color;
        }

        public int getNo2()
        {
            return this.no2;
        }

        public void setNo2(int no2)
        {
            this.no2 = no2;
        }

        public int getAqi()
        {
            return this.aqi;
        }

        public void setAqi(int aqi)
        {
            this.aqi = aqi;
        }

        public String getQuality()
        {
            return this.quality;
        }

        public void setQuality(String quality)
        {
            this.quality = quality;
        }

        public int getPm10()
        {
            return this.pm10;
        }

        public void setPm10(int pm10)
        {
            this.pm10 = pm10;
        }

        public int getPm25()
        {
            return this.pm25;
        }

        public void setPm25(int pm25)
        {
            this.pm25 = pm25;
        }

        public String getAdvice()
        {
            return this.advice;
        }

        public void setAdvice(String advice)
        {
            this.advice = advice;
        }

        public String getChief()
        {
            return this.chief;
        }

        public void setChief(String chief)
        {
            this.chief = chief;
        }

        public long getUpDateTime()
        {
            return this.upDateTime;
        }

        public void setUpDateTime(long upDateTime)
        {
            this.upDateTime = upDateTime;
        }
    }

    public static class WeatherBeanX
    {
        private String aqi;
        private String date;
        private InfoBeanX info;

        public String getAqi()
        {
            return this.aqi;
        }

        public void setAqi(String aqi)
        {
            this.aqi = aqi;
        }

        public String getDate()
        {
            return this.date;
        }

        public void setDate(String date)
        {
            this.date = date;
        }

        public InfoBeanX getInfo()
        {
            return this.info;
        }

        public void setInfo(InfoBeanX info)
        {
            this.info = info;
        }

        public static class InfoBeanX
        {
            private List<String> night;
            private List<String> day;

            public List<String> getNight()
            {
                return this.night;
            }

            public void setNight(List<String> night)
            {
                this.night = night;
            }

            public List<String> getDay()
            {
                return this.day;
            }

            public void setDay(List<String> day)
            {
                this.day = day;
            }
        }
    }

    public static class HourlyForecastBean
    {
        private String img;
        private String wind_speed;
        private String hour;
        private String wind_direct;
        private String temperature;
        private String info;

        public String getImg()
        {
            return this.img;
        }

        public void setImg(String img)
        {
            this.img = img;
        }

        public String getWind_speed()
        {
            return this.wind_speed;
        }

        public void setWind_speed(String wind_speed)
        {
            this.wind_speed = wind_speed;
        }

        public String getHour()
        {
            return this.hour;
        }

        public void setHour(String hour)
        {
            this.hour = hour;
        }

        public String getWind_direct()
        {
            return this.wind_direct;
        }

        public void setWind_direct(String wind_direct)
        {
            this.wind_direct = wind_direct;
        }

        public String getTemperature()
        {
            return this.temperature;
        }

        public void setTemperature(String temperature)
        {
            this.temperature = temperature;
        }

        public String getInfo()
        {
            return this.info;
        }

        public void setInfo(String info)
        {
            this.info = info;
        }
    }
}
