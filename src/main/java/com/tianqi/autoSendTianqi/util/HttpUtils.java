package com.tianqi.autoSendTianqi.util;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import java.io.InputStream;
import java.io.InputStreamReader;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.commons.httpclient.params.HttpConnectionManagerParams;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpUtils
{
    private static final Logger log = LoggerFactory.getLogger(HttpUtils.class);

    public static boolean doPost(String url, Object returnVo, String successValue, String successAttr)
    {
        String json = JSON.toJSONString(returnVo);
        boolean flag = false;
        for (int i = 0; i < 3; i++)
        {
            String result = httpPost(url, json);
            if (StringUtils.isNotBlank(result))
            {
                JSONObject jo = JSONObject.parseObject(result);
                log.info("接口的URL===入参为:" + json + "第  " + i + "  次http请求==返回值：" + jo.getString(successValue));
                if (successAttr.equals(jo.getString(successValue)))
                {
                    flag = true;
                    break;
                }
            }
        }
        return flag;
    }

    public static String httpPost(String url, String json)
    {
        PostMethod method = new PostMethod(url);
        HttpClient httpClient = new HttpClient();
        try
        {
            HttpConnectionManagerParams managerParams = httpClient.getHttpConnectionManager().getParams();
            managerParams.setConnectionTimeout(10000);
            managerParams.setSoTimeout(10000);
            RequestEntity entity = new StringRequestEntity(json, "application/json", "UTF-8");
            method.setRequestEntity(entity);
            int statusCode = httpClient.executeMethod(method);
            if (statusCode != 200)
            {
                log.error("HttpClient Error==[入参:" + json + "][statusCode = " + statusCode + "]");
                return "";
            }
            InputStream in = method.getResponseBodyAsStream();
            StringBuffer sb = new StringBuffer();
            InputStreamReader isr = new InputStreamReader(in, "UTF-8");
            char[] b = new char[4096];
            int n;
            while ((n = isr.read(b)) != -1)
            {

                sb.append(new String(b, 0, n));
            }
            String returnStr = sb.toString();
            return returnStr;
        }
        catch (Exception e)
        {
            log.error("====接口的URL失败======" + json);
            e.getStackTrace();
        }
        finally
        {
            method.releaseConnection();
        }
        return "";
    }

    public static String httpGet(String url)
    {
        GetMethod method = new GetMethod(url);
        HttpClient httpClient = new HttpClient();
        try
        {
            HttpConnectionManagerParams managerParams = httpClient.getHttpConnectionManager().getParams();
            managerParams.setConnectionTimeout(10000);
            managerParams.setSoTimeout(10000);
            int statusCode = httpClient.executeMethod(method);
            if (statusCode != 200)
            {
                log.error("HttpClient Error==[statusCode = " + statusCode + "]");
                return "";
            }
            InputStream in = method.getResponseBodyAsStream();
            StringBuffer sb = new StringBuffer();
            InputStreamReader isr = new InputStreamReader(in, "UTF-8");
            char[] b = new char[4096];
            int n;
            while ((n = isr.read(b)) != -1)
            {
                sb.append(new String(b, 0, n));
            }
            String returnStr = sb.toString();
            return returnStr;
        }
        catch (Exception e)
        {
            log.error("====接口的URL失败======");
            e.getStackTrace();
        }
        finally
        {
            method.releaseConnection();
        }
        return "";
    }
}
