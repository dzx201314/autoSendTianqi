package com.tianqi.autoSendTianqi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class AutoSendTianqiApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(AutoSendTianqiApplication.class, args);
    }
}
