<h1 align="center">AutoSendTianqi项目介绍</h1><br/>
# 背景
&nbsp;&nbsp;&nbsp;&nbsp;一直看python可以定时发微信消息，天气预报、小故事、情话什么的；上网搜有没有java版本的，下下来玩一玩，谁知道没有找到，只好自己写一个了，由于这是个自娱自乐的小程序，就没有这么多代码规范，大家莫嫌弃<br/>
<br/>
# 术语
<table border="1">
<tr bgcolor="#DBDBDB"><td>术语</td><td>解释</td></tr>
</table>
<br/>
# 模块
autoSendTianqi<br/>
<br/>
# 功能
- autoSendTianqi：实现了通过邮件、短信定时发天气情况。<br/>
<br/>

# 结果图

![邮件](img/email.png)

![短信](img/sms.png)

[回到顶部](#readme)